<?php
$my_file = 'test.txt';
$handle = fopen($my_file,'r');

$emplyeeArr = [];
$i = 0;

// traverse line by line of txt file
while (($employee = fgets($handle, 1024)) !== false) {

  $explodeArr = explode(" ", $employee);

  // generate a array from data of txt file

  $emplyeeArr[$i] = array(

      'id' => $explodeArr[0],
      'salary' => $explodeArr[1],
  );

  $i++;
}

// search logic
if( isset($_POST['search']) && !empty($_POST['search'])){

    foreach ($emplyeeArr as $key => $emplyee) {

      $result = 'Not Found';

      if($emplyee['id'] == $_POST['search']){

        $result = $emplyee['salary'];
        break;
      }
  }
}



$max = 0;
$min = isset($emplyeeArr[0]['salary']) ? $emplyeeArr[0]['salary'] : 0;
$totalSalary = 0;
$employCount = count($emplyeeArr);
$average = '';

foreach ($emplyeeArr as $key => $emplyee) {

      // Avarage Salary
      $totalSalary = $emplyee['salary'];
      $average += $totalSalary/$employCount;

      // max salary
      if ($max < $emplyee['salary']) {
        $max = $emplyee['salary'];
      }
      // min salary
      if ($min > $emplyee['salary']) {
        $min = $emplyee['salary'];
      }

  }

?>

<!DOCTYPE html>
<html>
<head>
  <title>Prove Your PHP Knowledge</title>
</head>
<body>

  <a href="https://careerclub.net/index.php/challenge/4/Prove+Your+PHP+Knowledge"> Problem Link</a>
  <h4>Avarage Salary: <?php echo $average; ?></h4>
  <h4>Maximum Salary: <?php echo $max; ?></h4>
  <h4>Minimun Salary: <?php echo $min; ?></h4>
  <form method="post">
    <label>Search Id:</label>
    <input type="text" name="search" placeholder="Search by ID">
    <input type="submit" name="submit" value="Show Salary">
  </form>
  <h4>Salary Amount: <?php echo isset($result) ? $result: "" ?></h4>
</body>
</html>
